package com.quarapay.q99.mock.template;

import com.quarapay.q99.mock.state.AbstractMockState;
import com.quarapay.q99.mock.state.universal.UniversalOkState;
import org.springframework.http.ResponseEntity;

/**
 * <b>Parent</b> for all mock services. <br>
 * Service can change its behavior by switching between available states.
 * @see AbstractMockState
 */
public abstract class ServiceTemplate extends AbstractMockState {
    private AbstractMockState state;

    public ServiceTemplate() {
        this.state = new UniversalOkState();
    }

    /**
     * Change mock service state by state name. Delegates this action to the child.
     * @param stateName Name of the state child will be looking for.
     * @return this
     */
    public abstract ServiceTemplate changeState(String stateName);

    /**
     * Change mock service state to specific implementation.
     * @param state State implementation.
     * @return this
     */
    public final ServiceTemplate changeState(AbstractMockState state) {
        this.state = state;
        return this;
    }

    /**
     * Complete request.
     * @param request Raw request body string.
     * @return Response.
     */
    @Override
    public ResponseEntity<Object> proceed(String request) {
        return state.proceed(request);
    }

    /**
     * Gets all services back to their initial state
     */
    public void refresh() {
        this.state = new UniversalOkState();
    }
}
