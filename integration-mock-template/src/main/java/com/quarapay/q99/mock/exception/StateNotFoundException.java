package com.quarapay.q99.mock.exception;

public class StateNotFoundException extends RuntimeException {
    public StateNotFoundException(Class<?> c, String message) {
        super(c.getName() + ": " + message);
    }
}
