package com.quarapay.q99.mock.state.universal;

import com.quarapay.q99.mock.state.AbstractMockState;
import org.springframework.http.ResponseEntity;

public class UniversalOkEchoState extends AbstractMockState {
    @Override
    public ResponseEntity<Object> proceed(String request) {
        return ResponseEntity.ok(request);
    }
}
