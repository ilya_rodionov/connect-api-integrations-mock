package com.quarapay.q99.mock.state.universal;

import com.quarapay.q99.mock.state.AbstractMockState;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class UniversalForbiddenState extends AbstractMockState {
    private final static String MESSAGE = "Forbidden!";

    @Override
    public ResponseEntity<Object> proceed(String request) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(MESSAGE);
    }
}
