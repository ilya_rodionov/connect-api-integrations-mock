package com.quarapay.q99.mock.state.universal;

import com.quarapay.q99.mock.state.AbstractMockState;
import org.springframework.http.ResponseEntity;

public class UniversalInternalServerErrorState extends AbstractMockState {
    private final static String MESSAGE = "Internal server error!";

    @Override
    public ResponseEntity<Object> proceed(String request) {
        return ResponseEntity.internalServerError().body(MESSAGE);
    }
}
