package com.quarapay.q99.mock.state.universal;

import com.quarapay.q99.mock.state.AbstractMockState;
import org.springframework.http.ResponseEntity;

public class UniversalOkState extends AbstractMockState {
    private final static String MESSAGE = "Ok";

    @Override
    public ResponseEntity<Object> proceed(String request) {
        return ResponseEntity.ok(MESSAGE);
    }
}
