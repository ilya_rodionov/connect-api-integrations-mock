package com.quarapay.q99.mock.state.universal;

import com.quarapay.q99.mock.state.AbstractMockState;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class UniversalBadRequestState extends AbstractMockState {
    private final static String MESSAGE = "Bad request!";

    @Override
    public ResponseEntity<Object> proceed(String request) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(MESSAGE);
    }
}
