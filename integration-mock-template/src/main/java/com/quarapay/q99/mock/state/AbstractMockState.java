package com.quarapay.q99.mock.state;

import org.springframework.http.ResponseEntity;

/**
 * <b>Parent</b> for all mock states. <br>
 * Each one state implements certain behavior depends on request.
 * @see com.quarapay.q99.mock.state.universal.UniversalOkState
 * @see com.quarapay.q99.mock.state.universal.UniversalInternalServerErrorState
 * @see com.quarapay.q99.mock.state.universal.UniversalForbiddenState
 * @see com.quarapay.q99.mock.state.universal.UniversalUnauthorizedState
 */
public abstract class AbstractMockState {
    private static final String OVERRIDE_MESSAGE = "Override proceed method!";

    /**
     * Complete request.
     * @param request Raw request body string.
     * @return Response.
     */
    public ResponseEntity<Object> proceed(String request) {
        return ResponseEntity.ok().body(OVERRIDE_MESSAGE);
    }
}
