package com.quarapay.q99.mock.kyc.state.mobile;

import com.quarapay.q99.mock.state.AbstractMockState;
import com.quarapay.q99.mock.state.universal.UniversalForbiddenState;
import com.quarapay.q99.mock.state.universal.UniversalInternalServerErrorState;
import com.quarapay.q99.mock.state.universal.UniversalOkEchoState;
import com.quarapay.q99.mock.state.universal.UniversalUnauthorizedState;

public enum KycMobileVerificationMockState {
    OK(new KycMobileVerificationOkState()),
    OK_ERROR(new KycMobileVerificationOkErrorState()),
    OK_ECHO(new UniversalOkEchoState()),
    FORBIDDEN(new UniversalForbiddenState()),
    SERVER_INTERNAL_ERROR(new UniversalInternalServerErrorState()),
    UNAUTHORIZED(new UniversalUnauthorizedState());

    private final AbstractMockState state;

    KycMobileVerificationMockState(AbstractMockState state) {
        this.state = state;
    }

    public AbstractMockState get() {
        return state;
    }
}
