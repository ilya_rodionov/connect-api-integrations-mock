package com.quarapay.q99.mock.kyc.service;

import com.quarapay.q99.mock.template.ServiceTemplate;

public abstract class KycServiceTemplate extends ServiceTemplate {

    public KycServiceTemplate() {
        init();
    }

    @Override
    public void refresh() {
        init();
    }

    /**
     * Set initial state of Service
     */
    protected abstract void init();
}
