package com.quarapay.q99.mock.kyc.state;

import com.quarapay.q99.mock.state.AbstractMockState;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.http.ResponseEntity;

import java.nio.charset.StandardCharsets;

public abstract class KycMockState extends AbstractMockState {

    private final String body;

    @SneakyThrows
    public KycMockState() {
        body = IOUtils.resourceToString(getResourcePath(), StandardCharsets.UTF_8, ClassLoader.getSystemClassLoader());
    }

    @Override
    public ResponseEntity<Object> proceed(String request) {
        return ResponseEntity.ok(body);
    }

    protected abstract String getResourcePath();
}
