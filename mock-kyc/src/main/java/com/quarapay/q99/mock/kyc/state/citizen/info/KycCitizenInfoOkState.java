package com.quarapay.q99.mock.kyc.state.citizen.info;

import com.quarapay.q99.mock.kyc.state.KycMockState;


public class KycCitizenInfoOkState extends KycMockState {

    @Override
    public String getResourcePath() {
        return "data/citizen/info/citizen-info-ok.json";
    }

}
