package com.quarapay.q99.mock.kyc.state.citizen.info;

import com.quarapay.q99.mock.kyc.state.KycMockState;


public class KycCitizenInfoOkErrorState extends KycMockState {

    @Override
    public String getResourcePath() {
        return "data/mobile/mobile-verification-ok-error.json";
    }

}
