package com.quarapay.q99.mock.kyc.state.otp;

import com.quarapay.q99.mock.kyc.state.KycMockState;


public class KycSendOtpOkErrorState extends KycMockState {

    @Override
    public String getResourcePath() {
        return "data/otp/send-otp-ok-error.json";
    }

}
