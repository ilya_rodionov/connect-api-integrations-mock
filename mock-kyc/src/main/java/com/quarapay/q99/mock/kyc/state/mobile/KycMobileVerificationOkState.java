package com.quarapay.q99.mock.kyc.state.mobile;

import com.quarapay.q99.mock.kyc.state.KycMockState;


public class KycMobileVerificationOkState extends KycMockState {

    @Override
    public String getResourcePath() {
        return "data/mobile/mobile-verification-ok.json";
    }

}
