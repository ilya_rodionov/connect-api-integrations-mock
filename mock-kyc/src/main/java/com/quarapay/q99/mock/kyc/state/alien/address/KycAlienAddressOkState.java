package com.quarapay.q99.mock.kyc.state.alien.address;

import com.quarapay.q99.mock.kyc.state.KycMockState;


public class KycAlienAddressOkState extends KycMockState {

    @Override
    public String getResourcePath() {
        return "data/citizen/address/citizen-address-ok.json";
    }
}
