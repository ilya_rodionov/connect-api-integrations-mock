package com.quarapay.q99.mock.kyc.service;

import com.quarapay.q99.mock.exception.StateNotFoundException;
import com.quarapay.q99.mock.kyc.state.alien.address.KycAlienAddressMockState;
import com.quarapay.q99.mock.template.ServiceTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service("KYC-ALIEN-ADDRESS")
public class KycAlienAddressMockService extends KycServiceTemplate {

    @Override
    public ServiceTemplate changeState(String stateName) {
        Objects.requireNonNull(stateName, "State name must be not null!");
        try {
            KycAlienAddressMockState state = KycAlienAddressMockState.valueOf(stateName.toUpperCase());
            changeState(state.get());
            return this;
        } catch (Exception e) {
            throw new StateNotFoundException(this.getClass(), stateName);
        }
    }

    @Override
    protected void init() {
        changeState(KycAlienAddressMockState.OK.get());
    }
}
