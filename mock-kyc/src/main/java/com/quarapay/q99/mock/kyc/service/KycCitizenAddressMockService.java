package com.quarapay.q99.mock.kyc.service;

import com.quarapay.q99.mock.exception.StateNotFoundException;
import com.quarapay.q99.mock.kyc.state.citizen.address.KycCitizenAddressMockState;
import com.quarapay.q99.mock.template.ServiceTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service("KYC-CITIZEN-ADDRESS")
public class KycCitizenAddressMockService extends KycServiceTemplate {

    @Override
    public ServiceTemplate changeState(String stateName) {
        Objects.requireNonNull(stateName, "State name must be not null!");
        try {
            KycCitizenAddressMockState state = KycCitizenAddressMockState.valueOf(stateName.toUpperCase());
            changeState(state.get());
            return this;
        } catch (Exception e) {
            throw new StateNotFoundException(this.getClass(), stateName);
        }
    }

    @Override
    protected void init() {
        changeState(KycCitizenAddressMockState.OK.get());
    }
}
