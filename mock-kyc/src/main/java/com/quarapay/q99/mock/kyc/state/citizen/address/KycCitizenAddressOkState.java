package com.quarapay.q99.mock.kyc.state.citizen.address;

import com.quarapay.q99.mock.kyc.state.KycMockState;


public class KycCitizenAddressOkState extends KycMockState {

    @Override
    public String getResourcePath() {
        return "data/citizen/address/citizen-address-ok.json";
    }

}
