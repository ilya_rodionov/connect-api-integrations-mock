package com.quarapay.q99.mock.kyc.state.otp;

import com.quarapay.q99.mock.state.AbstractMockState;
import com.quarapay.q99.mock.state.universal.UniversalForbiddenState;
import com.quarapay.q99.mock.state.universal.UniversalInternalServerErrorState;
import com.quarapay.q99.mock.state.universal.UniversalOkEchoState;
import com.quarapay.q99.mock.state.universal.UniversalUnauthorizedState;

public enum KycSendOtpMockState {
    OK(new KycSendOtpOkState()),
    OK_ERROR(new KycSendOtpOkErrorState()),
    OK_ECHO(new UniversalOkEchoState()),
    FORBIDDEN(new UniversalForbiddenState()),
    SERVER_INTERNAL_ERROR(new UniversalInternalServerErrorState()),
    UNAUTHORIZED(new UniversalUnauthorizedState());

    private final AbstractMockState state;

    KycSendOtpMockState(AbstractMockState state) {
        this.state = state;
    }

    public AbstractMockState get() {
        return state;
    }
}
