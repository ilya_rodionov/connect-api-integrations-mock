package com.quarapay.q99.mock.kyc.service;

import com.quarapay.q99.mock.exception.StateNotFoundException;
import com.quarapay.q99.mock.kyc.state.citizen.info.KycCitizenInfoMockState;
import com.quarapay.q99.mock.template.ServiceTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service("KYC-CITIZEN-INFO")
public class KycCitizenInfoMockService extends KycServiceTemplate {

    @Override
    public ServiceTemplate changeState(String stateName) {
        Objects.requireNonNull(stateName, "State name must be not null!");
        try {
            KycCitizenInfoMockState state = KycCitizenInfoMockState.valueOf(stateName.toUpperCase());
            changeState(state.get());
            return this;
        } catch (Exception e) {
            throw new StateNotFoundException(this.getClass(), stateName);
        }
    }

    @Override
    protected void init() {
        changeState(KycCitizenInfoMockState.OK.get());
    }
}
