package com.quarapay.q99.mock.kyc.state.citizen.address;

import com.quarapay.q99.mock.kyc.state.KycMockState;


public class KycCitizenAddressOkErrorState extends KycMockState {

    @Override
    public String getResourcePath() {
        return "data/citizen/address/citizen-address-ok-error.json";
    }
}
