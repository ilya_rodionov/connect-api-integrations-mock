package com.quarapay.q99.mock.kyc.state.alien.info;

import com.quarapay.q99.mock.kyc.state.KycMockState;


public class KycAlienInfoOkErrorState extends KycMockState {

    @Override
    public String getResourcePath() {
        return "data/alien/info/alien-info-ok-error.json";
    }
}
