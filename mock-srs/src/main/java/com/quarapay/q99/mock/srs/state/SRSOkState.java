package com.quarapay.q99.mock.srs.state;

import com.quarapay.q99.mock.state.AbstractMockState;
import org.apache.commons.io.IOUtils;
import org.springframework.http.ResponseEntity;

import java.nio.charset.StandardCharsets;

public class SRSOkState extends AbstractMockState {
    private final String body;

    public SRSOkState() {
        try {
            body = IOUtils.resourceToString("data/amlock_response_ok.xml",
                    StandardCharsets.UTF_8,
                    SRSOkState.class.getClassLoader());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ResponseEntity<Object> proceed(String request) {
        return ResponseEntity.ok(body);
    }
}
