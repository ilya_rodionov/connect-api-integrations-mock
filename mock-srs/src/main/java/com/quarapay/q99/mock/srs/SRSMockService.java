package com.quarapay.q99.mock.srs;

import com.quarapay.q99.mock.exception.StateNotFoundException;
import com.quarapay.q99.mock.template.ServiceTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service("SRS")
public class SRSMockService extends ServiceTemplate {
    public SRSMockService() {
        changeState(SRSMockState.OK.get());
    }

    @Override
    public ServiceTemplate changeState(String stateName) {
        Objects.requireNonNull(stateName, "State name must be not null!");

        try {
            SRSMockState state = SRSMockState.valueOf(stateName.toUpperCase());
            changeState(state.get());
            return this;
        } catch (Exception e) {
            throw new StateNotFoundException(this.getClass(), stateName);
        }
    }

    @Override
    public void refresh() {
        changeState(SRSMockState.OK.get());
    }
}
