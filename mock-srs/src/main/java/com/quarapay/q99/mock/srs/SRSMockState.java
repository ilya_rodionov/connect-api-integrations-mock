package com.quarapay.q99.mock.srs;

import com.quarapay.q99.mock.srs.state.SRSOkState;
import com.quarapay.q99.mock.state.AbstractMockState;
import com.quarapay.q99.mock.state.universal.UniversalForbiddenState;
import com.quarapay.q99.mock.state.universal.UniversalInternalServerErrorState;
import com.quarapay.q99.mock.state.universal.UniversalOkEchoState;
import com.quarapay.q99.mock.state.universal.UniversalUnauthorizedState;

public enum SRSMockState {
    OK(new SRSOkState()),
    OK_ECHO(new UniversalOkEchoState()),
    FORBIDDEN(new UniversalForbiddenState()),
    SERVER_INTERNAL_ERROR(new UniversalInternalServerErrorState()),
    UNAUTHORIZED(new UniversalUnauthorizedState());

    private final AbstractMockState state;

    SRSMockState(AbstractMockState state) {
        this.state = state;
    }

    public AbstractMockState get() {
        return state;
    }
}
