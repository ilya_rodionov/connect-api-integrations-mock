package com.quarapay.q99.mock.card.structure;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class HyperpayResponseModel {
    private ResultModel result;
    private String buildNumber;
    private String timestamp;
    private String ndc;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;
}
