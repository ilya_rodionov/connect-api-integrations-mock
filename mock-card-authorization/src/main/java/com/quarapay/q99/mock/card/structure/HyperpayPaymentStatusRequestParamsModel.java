package com.quarapay.q99.mock.card.structure;

import lombok.Data;

@Data
public class HyperpayPaymentStatusRequestParamsModel {
    private String entityId;
    private String checkoutId;
    private String authorization;
}
