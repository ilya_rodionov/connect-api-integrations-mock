package com.quarapay.q99.mock.card.hyperpay.service.stub;

import com.quarapay.q99.mock.card.hyperpay.state.stub.checkout.CardAuthorizationCheckoutMockState;
import com.quarapay.q99.mock.exception.StateNotFoundException;
import com.quarapay.q99.mock.template.ServiceTemplate;
import org.springframework.stereotype.Service;

@Service("CARD-CHECKOUT")
public class CardAuthorizationCheckoutMockService extends ServiceTemplate {
    public CardAuthorizationCheckoutMockService() {
        changeState(CardAuthorizationCheckoutMockState.OK.get());
    }

    @Override
    public ServiceTemplate changeState(String stateName) {
        try {
            CardAuthorizationCheckoutMockState state = CardAuthorizationCheckoutMockState
                    .valueOf(stateName.toUpperCase());
            changeState(state.get());
        } catch (Exception e) {
            throw new StateNotFoundException(this.getClass(), e.getMessage());
        }
        return this;
    }

    @Override
    public void refresh() {
        changeState(CardAuthorizationCheckoutMockState.OK.get());
    }
}
