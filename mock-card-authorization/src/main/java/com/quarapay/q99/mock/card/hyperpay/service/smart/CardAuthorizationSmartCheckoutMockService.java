package com.quarapay.q99.mock.card.hyperpay.service.smart;

import com.quarapay.q99.mock.card.hyperpay.state.smart.checkout.CardAuthorizationSmartCheckoutMockState;
import com.quarapay.q99.mock.exception.StateNotFoundException;
import com.quarapay.q99.mock.template.ServiceTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service("CARD-CHECKOUT-SMART")
public class CardAuthorizationSmartCheckoutMockService extends ServiceTemplate {
    @Autowired
    public CardAuthorizationSmartCheckoutMockService(CardAuthorizationSmartCheckoutMockState.CardAuthorizationSmartMockStateInjector injector) {
        injector.initialize();
    }

    @PostConstruct
    public void init() {
        refresh();
    }

    @Override
    public ServiceTemplate changeState(String stateName) {
        try {
            CardAuthorizationSmartCheckoutMockState state = CardAuthorizationSmartCheckoutMockState
                    .valueOf(stateName.toUpperCase());
            changeState(state.get());
        } catch (Exception e) {
            throw new StateNotFoundException(this.getClass(), e.getMessage());
        }
        return this;
    }

    @Override
    public void refresh() {
        changeState(CardAuthorizationSmartCheckoutMockState.SMART.get());
    }
}
