package com.quarapay.q99.mock.card.hyperpay.state.stub.status;

import com.quarapay.q99.mock.state.AbstractMockState;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.nio.charset.StandardCharsets;

public class CardAuthorizationPaymentStatusOkState extends AbstractMockState {
    @Override
    public ResponseEntity<Object> proceed(String request) {
        try {
            String rawResponse = IOUtils.resourceToString("data/prepare_payment_status_pending.json",
                    StandardCharsets.UTF_8,
                    ClassLoader.getSystemClassLoader());

            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(rawResponse);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().contentType(MediaType.TEXT_PLAIN).body(e);
        }
    }
}
