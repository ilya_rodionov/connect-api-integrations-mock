package com.quarapay.q99.mock.card.hyperpay.service.smart;

import org.apache.commons.collections4.map.PassiveExpiringMap;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Component
public class CardAuthorizationSmartStorage {
    private final static Object PLACEHOLDER = new Object();
    private final PassiveExpiringMap<String, Object> ids;

    public CardAuthorizationSmartStorage() {
        this.ids = new PassiveExpiringMap<>(TimeUnit.MINUTES.toMillis(30));
    }

    public void register(String key) {
        ids.remove(key);
        ids.put(key, PLACEHOLDER);
    }

    public boolean exists(String key) {
        Objects.requireNonNull(key, "Key must not be null!");
        return ids.containsKey(key);
    }
}
