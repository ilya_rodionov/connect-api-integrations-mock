package com.quarapay.q99.mock.card.hyperpay.state.stub.checkout;

import com.quarapay.q99.mock.state.AbstractMockState;
import com.quarapay.q99.mock.state.universal.UniversalBadRequestState;
import com.quarapay.q99.mock.state.universal.UniversalForbiddenState;
import com.quarapay.q99.mock.state.universal.UniversalInternalServerErrorState;
import com.quarapay.q99.mock.state.universal.UniversalOkEchoState;

public enum CardAuthorizationCheckoutMockState {
    OK(new CardAuthorizationCheckoutOkState()),
    OK_ECHO(new UniversalOkEchoState()),
    FORBIDDEN(new UniversalForbiddenState()),
    INTERNAL_SERVER_ERROR(new UniversalInternalServerErrorState()),
    BAD_REQUEST(new UniversalBadRequestState());

    private final AbstractMockState state;

    CardAuthorizationCheckoutMockState(AbstractMockState state) {
        this.state = state;
    }

    public AbstractMockState get() {
        return state;
    }
}
