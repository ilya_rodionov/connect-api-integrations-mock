package com.quarapay.q99.mock.card.structure;

import lombok.Data;

@Data
public class ResultModel {
    private String code;
    private String description;
}
