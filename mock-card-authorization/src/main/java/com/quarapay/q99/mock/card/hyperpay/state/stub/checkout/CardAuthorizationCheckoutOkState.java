package com.quarapay.q99.mock.card.hyperpay.state.stub.checkout;

import com.quarapay.q99.mock.state.AbstractMockState;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.nio.charset.StandardCharsets;

public class CardAuthorizationCheckoutOkState extends AbstractMockState {
    @Override
    public ResponseEntity<Object> proceed(String request) {
        try {
            String successfulRawResponse = IOUtils.resourceToString("data/prepare_checkout_successful.json",
                    StandardCharsets.UTF_8,
                    ClassLoader.getSystemClassLoader());
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(successfulRawResponse);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().contentType(MediaType.TEXT_PLAIN).body(e);
        }
    }
}
