package com.quarapay.q99.mock.card.hyperpay;

import com.quarapay.q99.mock.card.hyperpay.service.smart.CardAuthorizationSmartStorage;
import com.quarapay.q99.mock.card.hyperpay.state.smart.checkout.CardAuthorizationCheckoutSmartState;
import com.quarapay.q99.mock.card.hyperpay.state.smart.status.CardAuthorizationPaymentStatusSmartState;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class SmartConfiguration {
    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public CardAuthorizationCheckoutSmartState cardAuthorizationSmartState(CardAuthorizationSmartStorage storage) {
        return new CardAuthorizationCheckoutSmartState(storage);
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public CardAuthorizationPaymentStatusSmartState cardAuthorizationPaymentStatusSmartState(CardAuthorizationSmartStorage storage) {
        return new CardAuthorizationPaymentStatusSmartState(storage);
    }
}
