package com.quarapay.q99.mock.card.hyperpay.service.stub;

import com.quarapay.q99.mock.card.hyperpay.state.stub.status.CardAuthorizationPaymentStatusMockState;
import com.quarapay.q99.mock.exception.StateNotFoundException;
import com.quarapay.q99.mock.template.ServiceTemplate;
import org.springframework.stereotype.Service;

@Service("CARD-PAYMENT_STATUS")
public class CardAuthorizationPaymentStatusMockService extends ServiceTemplate {
    public CardAuthorizationPaymentStatusMockService() {
        changeState(CardAuthorizationPaymentStatusMockState.OK.get());
    }

    @Override
    public ServiceTemplate changeState(String stateName) {
        try {
            CardAuthorizationPaymentStatusMockState state = CardAuthorizationPaymentStatusMockState
                    .valueOf(stateName.toUpperCase());
            changeState(state.get());
        } catch (Exception e) {
            throw new StateNotFoundException(this.getClass(), e.getMessage());
        }
        return this;
    }

    @Override
    public void refresh() {
        changeState(CardAuthorizationPaymentStatusMockState.OK.get());
    }
}
