package com.quarapay.q99.mock.card.hyperpay.state.smart.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quarapay.q99.mock.card.hyperpay.service.smart.CardAuthorizationSmartStorage;
import com.quarapay.q99.mock.card.structure.HyperpayPaymentStatusRequestParamsModel;
import com.quarapay.q99.mock.card.structure.HyperpayResponseModel;
import com.quarapay.q99.mock.state.AbstractMockState;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.nio.charset.StandardCharsets;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CardAuthorizationPaymentStatusSmartState extends AbstractMockState {
    private final static String VALID_TOKEN = "OGE4Mjk0MTc0YjdlY2IyODAxNGI5Njk5MjIwMDE1Y2N8c3k2S0pzVDg=";
    private final CardAuthorizationSmartStorage storage;
    private final DateTimeFormatter formatter;
    private final ObjectMapper mapper;

    public CardAuthorizationPaymentStatusSmartState(CardAuthorizationSmartStorage storage) {
        this.storage = storage;
        this.formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ssZ");
        this.mapper = new ObjectMapper();
    }

    @Override
    public ResponseEntity<Object> proceed(String request) {
        try {
            HyperpayPaymentStatusRequestParamsModel model = mapper.readValue(request, HyperpayPaymentStatusRequestParamsModel.class);
            if (model.getAuthorization() == null) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
            if (!model.getAuthorization().trim().endsWith(VALID_TOKEN)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            if (model.getCheckoutId() == null || model.getEntityId() == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }

            String rawResponse;
            if (storage.exists(model.getCheckoutId())) {
                rawResponse = IOUtils.resourceToString("data/prepare_payment_status_pending.json",
                        StandardCharsets.UTF_8,
                        ClassLoader.getSystemClassLoader());
            } else {
                rawResponse = IOUtils.resourceToString("data/prepare_payment_status_no_session.json",
                        StandardCharsets.UTF_8,
                        ClassLoader.getSystemClassLoader());
            }

            HyperpayResponseModel responseModel = mapper.readValue(rawResponse, HyperpayResponseModel.class);
            responseModel.setTimestamp(ZonedDateTime.now(ZoneOffset.UTC).format(formatter));
            storage.register(responseModel.getId());
            return ResponseEntity.ok(responseModel);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().contentType(MediaType.TEXT_PLAIN).body(e);
        }
    }
}
