package com.quarapay.q99.mock.card.structure;

import lombok.Data;

@Data
public class HyperpayCheckoutRequestParamsModel {
    private String entityId;
    private Double amount;
    private String currency;
    private String paymentType;
    private String authorization;
}
