package com.quarapay.q99.mock.card.hyperpay.state.smart.status;

import com.quarapay.q99.mock.card.hyperpay.service.smart.CardAuthorizationSmartStorage;
import com.quarapay.q99.mock.state.AbstractMockState;
import com.quarapay.q99.mock.state.universal.UniversalBadRequestState;
import com.quarapay.q99.mock.state.universal.UniversalForbiddenState;
import com.quarapay.q99.mock.state.universal.UniversalInternalServerErrorState;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

public enum CardAuthorizationSmartPaymentStatusMockState {
    SMART(null),
    FORBIDDEN(new UniversalForbiddenState()),
    INTERNAL_SERVER_ERROR(new UniversalInternalServerErrorState()),
    BAD_REQUEST(new UniversalBadRequestState());

    private AbstractMockState state;

    CardAuthorizationSmartPaymentStatusMockState(AbstractMockState state) {
        this.state = state;
    }

    public AbstractMockState get() {
        return state;
    }

    private void inject(AbstractMockState state) {
        this.state = state;
    }

    @Component
    public static class CardAuthorizationSmartMockStateInjector {
        private final CardAuthorizationSmartStorage storage;
        private final ObjectProvider<CardAuthorizationPaymentStatusSmartState> provider;

        @Autowired
        public CardAuthorizationSmartMockStateInjector(CardAuthorizationSmartStorage storage,
                                                       ObjectProvider<CardAuthorizationPaymentStatusSmartState> provider) {
            this.storage = storage;
            this.provider = provider;
        }

        public void initialize() {
            SMART.inject(provider.getObject(storage));
        }
    }
}
