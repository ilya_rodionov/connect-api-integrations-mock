package com.quarapay.q99.mock.card.hyperpay.service.smart;

import com.quarapay.q99.mock.card.hyperpay.state.smart.status.CardAuthorizationSmartPaymentStatusMockState;
import com.quarapay.q99.mock.exception.StateNotFoundException;
import com.quarapay.q99.mock.template.ServiceTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service("CARD-PAYMENT_STATUS-SMART")
public class CardAuthorizationSmartPaymentStatusMockService extends ServiceTemplate {
    @Autowired
    public CardAuthorizationSmartPaymentStatusMockService(CardAuthorizationSmartPaymentStatusMockState.CardAuthorizationSmartMockStateInjector injector) {
        injector.initialize();
    }

    @PostConstruct
    public void init() {
        refresh();
    }

    @Override
    public ServiceTemplate changeState(String stateName) {
        try {
            CardAuthorizationSmartPaymentStatusMockState state = CardAuthorizationSmartPaymentStatusMockState
                    .valueOf(stateName.toUpperCase());
            changeState(state.get());
        } catch (Exception e) {
            throw new StateNotFoundException(this.getClass(), e.getMessage());
        }
        return this;
    }

    @Override
    public void refresh() {
        changeState(CardAuthorizationSmartPaymentStatusMockState.SMART.get());
    }
}
