package com.quarapay.q99.mock.controller;

import com.quarapay.q99.mock.mobile.MobileProviderMockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/mobile/unifonic")
public class MobileProviderController {
    private final MobileProviderMockService service;

    @Autowired
    public MobileProviderController(MobileProviderMockService service) {
        this.service = service;
    }

    @PostMapping(value = "/Unifonic/SendMsg", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> sendSms(@RequestBody String body) {
        return service.proceed(body);
    }
}
