package com.quarapay.q99.mock.controller;

import com.quarapay.q99.mock.kyc.service.KycAlienAddressMockService;
import com.quarapay.q99.mock.kyc.service.KycAlienInfoMockService;
import com.quarapay.q99.mock.kyc.service.KycCitizenAddressMockService;
import com.quarapay.q99.mock.kyc.service.KycCitizenInfoMockService;
import com.quarapay.q99.mock.kyc.service.KycMobileVerificationMockService;
import com.quarapay.q99.mock.kyc.service.KycSendOtpMockService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/kyc")
@RequiredArgsConstructor
public class KYCController {

    private final KycMobileVerificationMockService mobileVerificationMockService;
    private final KycSendOtpMockService kycSendOtpMockService;
    private final KycCitizenInfoMockService citizenInfoMockService;
    private final KycCitizenAddressMockService citizenAddressMockService;
    private final KycAlienInfoMockService alienInfoMockService;
    private final KycAlienAddressMockService alienAddressMockService;


    @PostMapping(value = "/Yakeen/MobileVerification", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> verifyMobile(@RequestBody String body) {
        return mobileVerificationMockService.proceed(body);
    }

    @PostMapping(value = "/OTPService/SendOTP", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> sendOTP(@RequestBody String body) {
        return kycSendOtpMockService.proceed(body);
    }

    @PostMapping(value = "/Yakeen/getCitizenInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getCitizenInfo(@RequestBody String body) {
        return citizenInfoMockService.proceed(body);
    }

    @PostMapping(value = "/Yakeen/getCitizenAddressInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getCitizenAddressInfo(@RequestBody String body) {
        return citizenAddressMockService.proceed(body);
    }

    @PostMapping(value = "/Yakeen/getAlienInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAlienInfo(@RequestBody String body) {
        return alienInfoMockService.proceed(body);
    }

    @PostMapping(value = "/Yakeen/getAlienAddressInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAlienAddressInfo() {
        return alienAddressMockService.proceed("");
    }
}
