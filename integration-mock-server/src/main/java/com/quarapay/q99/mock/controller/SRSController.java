package com.quarapay.q99.mock.controller;

import com.quarapay.q99.mock.srs.SRSMockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/srs/amlock")
public class SRSController {
    private final SRSMockService service;

    @Autowired
    public SRSController(SRSMockService service) {
        this.service = service;
    }

    @GetMapping(value = "/risk", produces = MediaType.TEXT_XML_VALUE)
    public ResponseEntity<Object> getSecurityRiskScoreResponse(
            @RequestParam(value = "body", required = false) String body) {
        return service.proceed(body);
    }
}
