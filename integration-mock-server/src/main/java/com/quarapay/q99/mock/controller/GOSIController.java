package com.quarapay.q99.mock.controller;

import com.quarapay.q99.mock.gosi.service.GOSIMockService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/gosi")
@RequiredArgsConstructor
public class GOSIController {

    private final GOSIMockService gosiMockService;


    @PostMapping("/Gosi/GetEmploymentInfo")
    public ResponseEntity<Object> getEmploymentInfo(@RequestBody String body) {
        return gosiMockService.proceed(body);
    }
}
