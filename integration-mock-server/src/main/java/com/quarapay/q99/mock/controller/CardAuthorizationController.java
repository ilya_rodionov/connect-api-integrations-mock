package com.quarapay.q99.mock.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quarapay.q99.mock.card.hyperpay.service.smart.CardAuthorizationSmartCheckoutMockService;
import com.quarapay.q99.mock.card.hyperpay.service.smart.CardAuthorizationSmartPaymentStatusMockService;
import com.quarapay.q99.mock.card.hyperpay.service.stub.CardAuthorizationCheckoutMockService;
import com.quarapay.q99.mock.card.hyperpay.service.stub.CardAuthorizationPaymentStatusMockService;
import com.quarapay.q99.mock.card.structure.HyperpayCheckoutRequestParamsModel;
import com.quarapay.q99.mock.card.structure.HyperpayPaymentStatusRequestParamsModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/card/hyperpay")
@RequiredArgsConstructor
public class CardAuthorizationController {
    private final ObjectMapper mapper = new ObjectMapper();
    private final CardAuthorizationSmartCheckoutMockService checkoutSmartService;
    private final CardAuthorizationSmartPaymentStatusMockService paymentStatusSmartService;
    private final CardAuthorizationCheckoutMockService checkoutService;
    private final CardAuthorizationPaymentStatusMockService paymentStatusService;

    @PostMapping("checkouts")
    public ResponseEntity<Object> checkouts(@RequestBody String body) {
        return checkoutService.proceed(body);
    }

    @PostMapping("smart/checkouts")
    public ResponseEntity<Object> checkouts(@RequestParam("entityId") String entityId,
                                            @RequestParam("amount") Double amount,
                                            @RequestParam("currency") String currency,
                                            @RequestParam("paymentType") String paymentType,
                                            @RequestHeader(value = "Authorization", required = false) String bearer)
            throws Exception {
        HyperpayCheckoutRequestParamsModel model = new HyperpayCheckoutRequestParamsModel();
        model.setEntityId(entityId);
        model.setAmount(amount);
        model.setCurrency(currency);
        model.setPaymentType(paymentType);
        model.setAuthorization(bearer);
        return checkoutSmartService.proceed(mapper.writeValueAsString(model));
    }

    @GetMapping("checkouts/{checkoutId}/payment")
    public ResponseEntity<Object> payment(@RequestParam(value = "body", required = false) String body) {
        return paymentStatusService.proceed(body);
    }

    @GetMapping("smart/checkouts/{checkoutId}/payment")
    public ResponseEntity<Object> payment(@PathVariable("checkoutId") String checkoutId,
                                          @RequestParam("entityId") String entityId,
                                          @RequestHeader(value = "Authorization", required = false) String bearer)
            throws Exception {
        HyperpayPaymentStatusRequestParamsModel model = new HyperpayPaymentStatusRequestParamsModel();
        model.setCheckoutId(checkoutId);
        model.setEntityId(entityId);
        model.setAuthorization(bearer);
        return paymentStatusSmartService.proceed(mapper.writeValueAsString(model));
    }
}
