package com.quarapay.q99.mock.controller;

import com.quarapay.q99.mock.template.ServiceTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/manage")
public class MockStatesController {
    private final Map<String, ServiceTemplate> services;

    @Autowired
    public MockStatesController(Map<String, ServiceTemplate> services) {
        this.services = Collections.unmodifiableMap(services);
    }

    @PostMapping(value = "/{service}/switch")
    public void changeState(@PathVariable("service") String serviceName,
                            @RequestParam("state") String stateName) {
        services.get(serviceName.toUpperCase()).changeState(stateName);
    }

    @PostMapping(value = "/refresh")
    public void refreshAllStates() {
        services.values().forEach(ServiceTemplate::refresh);
    }
}
