package com.quarapay.q99.mock.gosi.state;

public class GosiOkActiveState extends GosiGetEmploymentMockState {

    @Override
    protected String getResourcePath() {
        return "data/get-employment-info-ok-active.json";
    }
}
