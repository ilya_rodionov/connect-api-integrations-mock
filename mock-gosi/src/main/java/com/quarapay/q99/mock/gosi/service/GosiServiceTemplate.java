package com.quarapay.q99.mock.gosi.service;

import com.quarapay.q99.mock.template.ServiceTemplate;

public abstract class GosiServiceTemplate extends ServiceTemplate {

    public GosiServiceTemplate() {
        init();
    }

    @Override
    public void refresh() {
        init();
    }

    /**
     * Set initial state of Service
     */
    protected abstract void init();
}
