package com.quarapay.q99.mock.gosi.service;

import com.quarapay.q99.mock.exception.StateNotFoundException;
import com.quarapay.q99.mock.gosi.state.GosiMockState;
import com.quarapay.q99.mock.template.ServiceTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service("GOSI")
public class GOSIMockService extends GosiServiceTemplate {

    @Override
    public ServiceTemplate changeState(String stateName) {
        Objects.requireNonNull(stateName, "State name must be not null!");
        try {
            GosiMockState state = GosiMockState.valueOf(stateName.toUpperCase());
            changeState(state.get());
            return this;
        } catch (Exception e) {
            throw new StateNotFoundException(this.getClass(), stateName);
        }
    }

    @Override
    protected void init() {
        changeState(GosiMockState.OK.get());
    }
}
