package com.quarapay.q99.mock.gosi.state;

import com.quarapay.q99.mock.state.AbstractMockState;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.http.ResponseEntity;

import java.nio.charset.StandardCharsets;

public abstract class GosiGetEmploymentMockState extends AbstractMockState {

    private final String body;

    @SneakyThrows
    public GosiGetEmploymentMockState() {
        body = IOUtils.resourceToString(getResourcePath(), StandardCharsets.UTF_8, ClassLoader.getSystemClassLoader());
    }

    @Override
    public ResponseEntity<Object> proceed(String request) {
        return ResponseEntity.ok(body);
    }

    protected abstract String getResourcePath();
}
