package com.quarapay.q99.mock.mobile;

import com.quarapay.q99.mock.exception.StateNotFoundException;
import com.quarapay.q99.mock.template.ServiceTemplate;
import org.springframework.stereotype.Service;

@Service("MOBILE")
public class MobileProviderMockService extends ServiceTemplate {

    public MobileProviderMockService() {
        changeState(MobileProviderMockState.OK.get());
    }

    @Override
    public ServiceTemplate changeState(String stateName) {
        try {
            MobileProviderMockState state = MobileProviderMockState.valueOf(stateName.toUpperCase());
            return changeState(state.get());
        } catch (Exception e) {
            throw new StateNotFoundException(this.getClass(), stateName);
        }
    }

    @Override
    public void refresh() {
        changeState(MobileProviderMockState.OK.get());
    }
}
