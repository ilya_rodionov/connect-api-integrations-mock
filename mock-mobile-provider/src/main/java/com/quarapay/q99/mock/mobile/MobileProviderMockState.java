package com.quarapay.q99.mock.mobile;

import com.quarapay.q99.mock.mobile.state.MobileProviderOkState;
import com.quarapay.q99.mock.state.AbstractMockState;
import com.quarapay.q99.mock.state.universal.UniversalBadRequestState;
import com.quarapay.q99.mock.state.universal.UniversalForbiddenState;
import com.quarapay.q99.mock.state.universal.UniversalInternalServerErrorState;
import com.quarapay.q99.mock.state.universal.UniversalOkEchoState;
import com.quarapay.q99.mock.state.universal.UniversalUnauthorizedState;

enum MobileProviderMockState {
    OK(new MobileProviderOkState()),
    OK_ECHO(new UniversalOkEchoState()),
    FORBIDDEN(new UniversalForbiddenState()),
    SERVER_INTERNAL_ERROR(new UniversalInternalServerErrorState()),
    UNAUTHORIZED(new UniversalUnauthorizedState()),
    BAD_REQUEST(new UniversalBadRequestState());

    private final AbstractMockState state;

    MobileProviderMockState(AbstractMockState state) {
        this.state = state;
    }

    public AbstractMockState get() {
        return state;
    }
}
