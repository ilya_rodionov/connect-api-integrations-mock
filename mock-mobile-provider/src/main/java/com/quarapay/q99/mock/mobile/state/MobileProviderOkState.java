package com.quarapay.q99.mock.mobile.state;

import com.quarapay.q99.mock.state.AbstractMockState;
import org.apache.commons.io.IOUtils;
import org.springframework.http.ResponseEntity;

import java.nio.charset.StandardCharsets;

public class MobileProviderOkState extends AbstractMockState {
    private final String body;

    public MobileProviderOkState() {
        try {
            body = IOUtils.resourceToString("data/mobile-provider-ok.json",
                    StandardCharsets.UTF_8,
                    MobileProviderOkState.class.getClassLoader());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ResponseEntity<Object> proceed(String request) {
        return ResponseEntity.ok(body);
    }
}
